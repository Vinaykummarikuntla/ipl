"""importing csv file"""
import csv
import matplotlib.pyplot as plt
# import matplotlib.pyplot as plt

def calculate_no_of_matches(raw_data):
    """ # collecting the .csv files and converting into data"""
    
    with open(raw_data, "r", encoding='UTF-8') as file:
        csv_file = csv.DictReader(file)
        ipl_matches = list(csv_file)


    no_of_years = []
    for year in ipl_matches:
        no_of_years.append(year["season"])

    no_of_years = sorted(list(set(no_of_years)))

    # print(no_of_years)
    no_of_matches = [0]*len(no_of_years)
    result = dict(zip(no_of_years,no_of_matches))
    # print(result)

    for no_of_years in ipl_matches:
        result[no_of_years["season"]] =result[no_of_years["season"]] + 1

    # print(result)
    return result


def plotbarchart(result):
    """docstring"""
    matches_played=list(result.keys())
    year=list(result.values())
    plt.bar(matches_played,year,color='green',width=0.5)
    plt.xlabel("years")
    plt.ylabel("No. of matches played")
    plt.title("Number of matches played per year in IPL")
    plt.show()


def main():
    """docstring"""
    raw_data="/home/vinaykumar/Downloads/IPL-data-set-analytics-main/matches.csv"
    numberofmatchesplayed_peryear=calculate_no_of_matches(raw_data)
    plotbarchart(numberofmatchesplayed_peryear)


if __name__ == "__main__":
    main()
