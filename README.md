<<<<<<< README.md
# IPL-data-set-analytics

## Aim
This repository will collect data in csv format and process it to display bar plots using matplotlib.
## 1.How to get the data
The datasets can downloaded from the repo.


The dataset names are


**1.matches.csv**

**2.deliveries.csv**

**3.allumpire.csv**
## 2.Requirements
All the requirements and dependencies store in requirements.txt and install all requirements and dependencies run


```pip3 install -r requirements.txt```

## 3.Run Project
**1. Total runs scored by team**

* Click the file name
``` totalruns.py```

**2. Top batsman for Royal Challengers Bangalore**

* Click the file name
``` topbatsman.py```

**3. Foreign umpire analysis**

* Click the file name
``` foreign.py```

**4. Stacked chart of matches played by team by season**

* Click the file name
``` stackedchart.py```

**5.Number of matches played per year for all the years in IPL.**
 
* Click the file name
``` noofmatchesplayedperyear.py```  

**6. Number of matches won per team per year in IPL**

* Click the file name
``` noofmatchedwonperteam.py```  

**7. Extra runs conceded per team in the year 2016**

* Click the file name
``` extrarunscocededyear2016.py```

**8.  Top 10 economical bowlers in the year 2015**

* Click the file name
``` top10economicalbowlers.py```







