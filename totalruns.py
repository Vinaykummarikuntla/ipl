'''Total Scores of
All Teams '''
import csv
from matplotlib import pyplot as plt
data = {}
with open(r"/home/vinaykumar/Downloads/IPL-data-set-analytics-main/deliveries.csv",encoding='utf-8')as csv_file:
    csv_read = csv.reader(csv_file)
    next(csv_read)
    for index in csv_read:
        team = index[2]
        totalscore = int(index[17])
        if team in data:
            data[team] += totalscore
        else:
            data[team] = totalscore
teams = list(data.keys())
scores = list(data.values())
font1 = {"family": "serif", "color": "darkred", "size": 20}
font2 = {"family": "serif", "color": "darkred", "size": 15}
plt.barh(teams, scores)
plt.title("Total Scores of  All Teams", fontdict=font1)
plt.xlabel("Teams", fontdict=font2)
plt.ylabel("Scores", fontdict=font2)

plt.show()
