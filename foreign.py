'''Number of Umpires from
each country'''
import csv

from matplotlib import pyplot as plt

data = {}
with open(r"/home/vinaykumar/Downloads/IPL-data-set-analytics-main/allumpire.csv", encoding='utf-8') as csv_file:
    csv_read = csv.reader(csv_file)
    next(csv_read)
    for index in csv_read:
        country = index[0]
        if country != " India":
            print(country)
            if country in data:
                data[country] += 1
            else:
                data[country] = 1

country = list(data.keys())
no_of_umpires = list(data.values())
print(sum(no_of_umpires))
font1 = {"family": "serif", "color": "darkred", "size": 20}
font2 = {"family": "serif", "color": "darkred", "size": 15}
plt.barh(country, no_of_umpires)
plt.title("No of Umpires from each country", fontdict=font1)
plt.xlabel("Country", fontdict=font2)
plt.ylabel("No of Umpires", fontdict=font2)
plt.show()
