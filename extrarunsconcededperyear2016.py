"""importing csv file"""
import csv
import matplotlib.pyplot as plt

def rawdata(filename):
    """ # collecting the .csv files and converting into data"""
    with open(filename, "r", encoding='UTF-8') as file:
        csv_file = csv.DictReader(file)
        data = list(csv_file)
    return data


def calculate_runs(matches,deliveries):
    """doc string"""

    # importing the file of deliveries.csv and converting them into data
    deliveries_data = rawdata(deliveries)
    matches_data = rawdata(matches)
    teams = []
    for data in deliveries_data:
        teams.append(data["batting_team"])

    teams = list(set(teams))
    values = [0]*len(teams)
    team_runs = dict(zip(teams, values))

    matchid = []
    for ids in matches_data:

        if ids["season"] == "2016":
            matchid.append(ids["id"])

    #print(id)

    for data in deliveries_data:

        if data["match_id"] in matchid:
            team_runs[data["batting_team"]] += int(data["extra_runs"])

    return team_runs


def plot_extraruns(team_runs):
    """docstring"""
    runs=list(team_runs.keys())
    perteam=list(team_runs.values())
    plt.barh(runs,perteam)
    plt.show()


def main():
    """docstring"""
    matches = "/home/vinaykumar/Downloads/IPL-data-set-analytics-main/matches.csv"
    deliveries= "/home/vinaykumar/Downloads/IPL-data-set-analytics-main/deliveries.csv"
    team_runs = calculate_runs(matches,deliveries)
    plot_extraruns(team_runs)



main()
    