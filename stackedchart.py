'''stacked
chart'''
import csv

from matplotlib import pyplot as plt

data = {}
emptylist = []
with open(r"/home/vinaykumar/Downloads/IPL-data-set-analytics-main/matches.csv", encoding='utf-8') as csv_file:
    csv_read = csv.reader(csv_file)
    next(csv_read)
    for index in csv_read:
        season = index[1]
        team1 = index[4]
        team2 = index[5]
        if team1 not in emptylist:
            emptylist.append(team1)
        if team2 not in emptylist:
            emptylist.append(team2)
        if season in data:

            if team1 in data[season]:

                data[season][team1] += 1
            else:
                data[season][team1] = 1
            if team2 in data[season]:

                data[season][team2] += 1
            else:
                data[season][team2] = 1
        else:
            data[season] = {team1: 1, team2: 1}
listkeys = list(data.keys())
listkeys.sort()
m = []

for index in listkeys:
    f = []
    for item in emptylist:
        if item in data[index]:
            f.append(data[index][item])
        else:
            f.append(0)
    m.append(f)
z = []
clr = [
    "lime",
    "purple",
    "black",
    "green",
    "olive",
    "red",
    "yellow",
    "blue",
    "teal",
    "aqua",
    "navy",
    "grey",
    "fuchsia",
    "maroon",
]
p = 0
for index in range(len(m[0])):
    temp = []
    for item in range(len(m)):
        temp.append(m[item][index])
    if z == []:
        plt.bar(listkeys, temp, color=clr[p])
        p += 1
        z = temp
    else:
        plt.bar(listkeys, temp, bottom=z, color=clr[p])
        p += 1
        for index in range(len(temp)):
            z[index] = z[index] + temp[index]
for index in listkeys:
    print(index, data[index])
plt.xlabel("Year")
plt.ylabel("No of games played by team")
plt.legend(emptylist)
plt.title("No of games played by team by season")
plt.show()
