"""importing csv file"""
import csv
import matplotlib.pyplot as plt
import pandas as pd

def calculate_noofmatcheswon(ipldata):
    """ # collecting the .csv files and converting into data"""
    
    with open(ipldata, "r", encoding='UTF-8') as file:
        csv_file = csv.DictReader(file)
        ipl_matches = list(csv_file)
    years = []
    teamswon = []
    for season in ipl_matches:
        years.append(season["season"])
        teamswon.append(season["team1"])
    years = sorted(list(set(years)))
    teamswon = sorted(list(set(teamswon)))

    yearsdata = {}

    for year in years:
        teamsdata = {}
        for team in teamswon:
            teamsdata[team] = 0

        yearsdata[year] = teamsdata

    for match in ipl_matches:
        try :
            year = match["season"]
            team = match["winner"]

            yearsdata[year][team] += 1
        except KeyError :
            pass

    # print(yearsdata)
    return yearsdata, teamswon


def plotstackedchart(yearsdata ,teamswon):
    """docstring"""
    print("hello")
    chartlist = []
    seasons = list(yearsdata.keys())
    count = 0
    seasonsdata = list(yearsdata.values())
    for eachitem in seasonsdata:

        valuesofchart = [seasons[count]]+list(eachitem.values())
        chartlist.append(valuesofchart)
        count=count+1


    stackedchart = pd.DataFrame(chartlist,columns=["Teams"]+teamswon)
    stackedchart.plot(x='Teams', kind='bar', stacked=True,title='Stacked Bar Graph of matches per teams per season')
    plt.show()

def main():
    """docstring"""
    ipldata="/home/vinaykumar/Downloads/IPL-data-set-analytics-main/matches.csv"

    wondata, teamswon = calculate_noofmatcheswon(ipldata)

    plotstackedchart(wondata, teamswon)




main()
