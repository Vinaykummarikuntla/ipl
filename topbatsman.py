'''Total Scores of
Royal Challengers Banglore'''
import csv

from matplotlib import pyplot as plt

data = {}
with open(r"/home/vinaykumar/Downloads/IPL-data-set-analytics-main/deliveries.csv",encoding='utf-8') as csv_file:
    csv_read = csv.reader(csv_file)
    next(csv_read)
    for index in csv_read:
        team = index[2]
        batsman = index[6]
        score = int(index[17])
        if team == "Royal Challengers Bangalore":
            if batsman in data:
                data[batsman] += score
            else:
                data[batsman] = score
sortedvalues = sorted(data.values(), reverse=True)
result = {}
for index in sortedvalues[:10]:
    for item in data:
        if data[item] == index:
            result[item] = index
            break
players = list(result.keys())
scores = list(result.values())
print(players)
font1 = {"family": "serif", "color": "darkred", "size": 20}
font2 = {"family": "serif", "color": "darkred", "size": 15}
plt.barh(players, scores)
plt.title("Total Scores of Royal challengers Banglore", fontdict=font1)
plt.xlabel("Players", fontdict=font2)
plt.ylabel("Scores", fontdict=font2)
plt.show()
